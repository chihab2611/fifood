package com.fifood.app.fifood.rest;

import com.fifood.app.fifood.entity.Place;
import com.fifood.app.fifood.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class RestPlaceController {
    
    @Autowired
    PlaceService placeService;

    @GetMapping(value = "/{id}")
    public Place getUserById(@PathVariable("id") Long id){
        return placeService.findPlaceById(id);
    }

    @GetMapping(value = "/all")
    public Iterable<Place> getAllUser(){
        return placeService.findAllPlace();
    }

    @PostMapping(value="/add")
    public void addPlace(@RequestBody Place place){
        placeService.createPlace(place);
    }

    @GetMapping(value = "/remove/{id}")
    public  void removePlace(@PathVariable("id") Long id){
        placeService.deletePlace(id);
    }
}

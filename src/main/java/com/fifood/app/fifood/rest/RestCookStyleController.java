package com.fifood.app.fifood.rest;

import com.fifood.app.fifood.entity.CookStyle;
import com.fifood.app.fifood.service.CookStyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class RestCookStyleController {
    @Autowired
    CookStyleService cookStyleService;

    @GetMapping(value = "/{id}")
    public CookStyle getUserById(@PathVariable("id") Long id){
        return cookStyleService.findCookStyleById(id);
    }

    @GetMapping(value = "/all")
    public Iterable<CookStyle> getAllUser(){
        return cookStyleService.findAllCookStyle();
    }

    @PostMapping(value="/add")
    public void addCookStyle(@RequestBody CookStyle cookStyle){
        cookStyleService.createCookStyle(cookStyle);
    }

    @GetMapping(value = "/remove/{id}")
    public  void removeCookStyle(@PathVariable("id") Long id){
        cookStyleService.deleteCookStyle(id);
    }
}

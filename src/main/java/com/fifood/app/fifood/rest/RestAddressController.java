package com.fifood.app.fifood.rest;

import com.fifood.app.fifood.entity.Address;
import com.fifood.app.fifood.entity.User;
import com.fifood.app.fifood.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value="/api/address")
public class RestAddressController {
    @Autowired
    private AddressService addressService;

    @GetMapping(value = "/{id}")
    public Address getUserById(@PathVariable("id") Long id){
        return addressService.findAddressById(id);
    }

    @GetMapping(value = "/all")
    public Iterable<Address> getAllUser(){
        return addressService.findAllAddress();
    }

    @PostMapping(value="/add")
    public void addAddress(@RequestBody Address address){
        addressService.createAddress(address);
    }

    @GetMapping(value = "/remove/{id}")
    public  void removeAddress(@PathVariable("id") Long id){
        addressService.deleteAddress(id);
    }
}



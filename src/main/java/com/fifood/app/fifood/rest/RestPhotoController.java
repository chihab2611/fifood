package com.fifood.app.fifood.rest;

import com.fifood.app.fifood.entity.Photo;
import com.fifood.app.fifood.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class RestPhotoController {
    @Autowired
    PhotoService photoService;

    @GetMapping(value = "/{id}")
    public Photo getUserById(@PathVariable("id") Long id){
        return photoService.findPhotoById(id);
    }

    @GetMapping(value = "/all")
    public Iterable<Photo> getAllUser(){
        return photoService.findAllPhoto();
    }

    @PostMapping(value="/add")
    public void addPhoto(@RequestBody Photo photo){
        photoService.createPhoto(photo);
    }

    @GetMapping(value = "/remove/{id}")
    public  void removePhoto(@PathVariable("id") Long id){
        photoService.deletePhoto(id);
    }
}

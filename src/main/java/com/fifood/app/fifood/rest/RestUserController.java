package com.fifood.app.fifood.rest;

import com.fifood.app.fifood.entity.User;
import com.fifood.app.fifood.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class RestUserController {
    @Autowired
    UserService userService;

    @GetMapping(value = "/{id}")
    public User getUserById(@PathVariable("id") Long id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/all")
    public Iterable<User> getAllUser(){
        return userService.findAllUser();
    }

    @PostMapping(value="/add")
    public void addUser(@RequestBody User user){
        userService.createUser(user);
    }

    @GetMapping(value = "/remove/{id}")
    public  void removeUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
    }
}

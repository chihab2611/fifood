package com.fifood.app.fifood.repository;

import com.fifood.app.fifood.entity.CookStyle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookStyleRepository extends CrudRepository<CookStyle, Long> {

    CookStyle findCookStyleById(Long id);
}

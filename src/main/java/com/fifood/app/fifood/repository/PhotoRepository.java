package com.fifood.app.fifood.repository;

import com.fifood.app.fifood.entity.Photo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepository extends CrudRepository<Photo, Long> {

    Photo findPhotoById(Long id);
}

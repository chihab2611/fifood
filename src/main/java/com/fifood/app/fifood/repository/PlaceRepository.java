package com.fifood.app.fifood.repository;

import com.fifood.app.fifood.entity.Place;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository  extends CrudRepository<Place, Long> {

    Place findPlaceById(Long id);
}

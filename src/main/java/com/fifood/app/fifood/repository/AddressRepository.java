package com.fifood.app.fifood.repository;

import com.fifood.app.fifood.entity.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

    Address findAddressById(Long id);


}

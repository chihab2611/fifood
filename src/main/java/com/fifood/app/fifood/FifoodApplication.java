package com.fifood.app.fifood;

import com.fifood.app.fifood.entity.Address;
import com.fifood.app.fifood.entity.Place;
import com.fifood.app.fifood.entity.User;
import com.fifood.app.fifood.repository.AddressRepository;
import com.fifood.app.fifood.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FifoodApplication {

    public static void main(String[] args) {
        SpringApplication.run(FifoodApplication.class, args);
    }

}

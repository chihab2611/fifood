package com.fifood.app.fifood.service;

import com.fifood.app.fifood.entity.Photo;
import com.fifood.app.fifood.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotoService {
    @Autowired
    PhotoRepository photoRepository;

    public Photo findPhotoById(Long id){
        return photoRepository.findPhotoById(id);
    }

    public Iterable<Photo> findAllPhoto(){
        return photoRepository.findAll();
    }

    public void createPhoto(Photo photo){
        photoRepository.save(photo);
    }

    public void deletePhoto(Long id){
        photoRepository.deleteById(id);
    }
}

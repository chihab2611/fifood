package com.fifood.app.fifood.service;

import com.fifood.app.fifood.entity.User;
import com.fifood.app.fifood.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User findUserById(Long id){
        return userRepository.findUserById(id);
    }

    public Iterable<User> findAllUser(){
        return userRepository.findAll();
    }

    public void createUser(User user){
        userRepository.save(user);
    }

    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

}

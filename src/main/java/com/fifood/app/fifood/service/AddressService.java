package com.fifood.app.fifood.service;

import com.fifood.app.fifood.entity.Address;
import com.fifood.app.fifood.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;
    
    public Address findAddressById(Long id){
        return addressRepository.findAddressById(id);
    }

    public Iterable<Address> findAllAddress(){
        return addressRepository.findAll();
    }

    public void createAddress(Address address){
        addressRepository.save(address);
    }

    public void deleteAddress(Long id){
        addressRepository.deleteById(id);
    }


}

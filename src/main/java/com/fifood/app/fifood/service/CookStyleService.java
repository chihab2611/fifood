package com.fifood.app.fifood.service;

import com.fifood.app.fifood.entity.CookStyle;
import com.fifood.app.fifood.repository.CookStyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CookStyleService {
    @Autowired
    CookStyleRepository cookStyleRepository;

    public CookStyle findCookStyleById(Long id){
        return cookStyleRepository.findCookStyleById(id);
    }

    public Iterable<CookStyle> findAllCookStyle(){
        return cookStyleRepository.findAll();
    }

    public void createCookStyle(CookStyle cookStyle){
        cookStyleRepository.save(cookStyle);
    }

    public void deleteCookStyle(Long id){
        cookStyleRepository.deleteById(id);
    }
}

package com.fifood.app.fifood.service;

import com.fifood.app.fifood.entity.Place;
import com.fifood.app.fifood.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaceService {
    @Autowired
    PlaceRepository placeRepository;

    public Place findPlaceById(Long id){
        return placeRepository.findPlaceById(id);
    }

    public Iterable<Place> findAllPlace(){
        return placeRepository.findAll();
    }

    public void createPlace(Place place){
        placeRepository.save(place);
    }

    public void deletePlace(Long id){
        placeRepository.deleteById(id);
    }
}

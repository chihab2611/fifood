package com.fifood.app.fifood.entity;

import javax.persistence.*;

@Entity
public class CookStyle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long name;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "place_fk")
    private Place place;

    public CookStyle() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getName() {
        return name;
    }

    public void setName(Long name) {
        this.name = name;
    }
}

package com.fifood.app.fifood.entity;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String country;
    private int zipCode;
    private String street;
    private String number;

    @OneToOne
    @JoinColumn(name="place_fk")
    private Place place;

    public Long getId() {
        return id;
    }

    public Address() {
    }

    public Address(String country, int zipCode, String street, String number) {
        this.country = country;
        this.zipCode = zipCode;
        this.street = street;
        this.number = number;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
